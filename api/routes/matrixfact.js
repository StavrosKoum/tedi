
const factorsNo = 19;
const MIN = 1;
const MAX = 5;
const a = 0.01;
const ITERATIONS = 50;
const multiply = require("mathjs");
const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Room = require("../models/room");
const Rating = require("../models/rating");
const User = require("../models/user");


router.get("/:userId", (req, res, next) => {
	const id = req.params.userId;
	arrayStuff(id)
		.then((array) => {
			const response = { recommendations: array };
			res.status(200).json(response);
		})
		.catch((e) => {
			res.status(401).json(e);
		});
});

async function arrayStuff(userId) {
	// user - factor array
	var U = Array.from(
		{ length: factorsNo },
		() => parseFloat((Math.random() * 2 - 1).toFixed(2)) //parseFloat((Math.random() * 5 + 1).toFixed(2)) //Math.floor(Math.random() * 5) + 1
	);
	const expected = []; //an array with the non null element of the UR array and their index
	// console.log("USER ", U);
	var rate;
	var num;
	var VTcolumn = [];
	var Urow = [];
	// factor - item array
	const VT = await getVT();
	// user - item array
	const [UI, id] = await getUI(userId);
	// console.log("u", U, "vt", VT, "ui", UI);

	//take the actual rating
	UI.forEach((element, index) => {
		if (element) {
			expected.push({ index: index, value: element }); //expected.push({ index: index - 1, value: element });
			num = index;
			rate = element;
			// console.log(index, "----", element);
		}
	});
	// console.log("Actual", expected[0].value);
	// console.log("Actual val possition ", expected[0].index);

	//create u row array
	U.forEach((row) => {
		Urow.push(row);
		// console.log("U ROW array", row);
	});

	var a = 0.01;
	for (let iter = 0; iter < ITERATIONS; iter++) {
		// train for 100 iterations
		for (let j = 0; j < expected.length; j++) {
			// iterate all ratings

			//step 4-take a prediction
			var predicted = 0;
			for (var i = 0; i < Urow.length; i++) {
				predicted += Urow[i] * VT[i][expected[j].index];
			}

			predicted = parseFloat(predicted.toFixed(2));
			// console.log("Prediction", predicted);

			//calculate the error
			var sub = expected[j].value - predicted;
			error = parseFloat(Math.pow(sub, 2).toFixed(2));
			if (iter % 20 === 0) {
				console.log("sub: ", sub);
				console.log("error: ", error);
				console.log("  exprected: ", expected[j].value);
				console.log("  predicted: ", predicted);
			}
			for (let i = 0; i < factorsNo; i++) {
				Urow[i] =
					Urow[i] +
					a *
						2 *
						(expected[j].value - predicted) *
						VT[i][expected[j].index];
			}
		}
	}
	//compute the formula U(i)orV(i) + ⍺x2(actual-predicted) x V(i)orU(i)
	var formula;
	var i = 0;

	

	//update VT matrix
	for (var i = 0; i < VT.length; i++) {
		VT[i][expected[0].index] = VTcolumn[i];
	}
	// console.log("VTTT", VT);

	//make a new array with predicted ratings
	var pred_ratings = [];
	var pred;
	//there are no 2d arrays in js so i take the length of the first array of the array
	//if any of this makes sense
	//anyway i take the right num, the num of items we have so thats why vt[0].length
	for (
		var i = 0;
		i < VT[0].length;
		i++ //items loop
	) {
		pred = 0;
		for (var k = 0; k < factorsNo; k++) {
			pred += Urow[k] * VT[k][i];
		}
		pred_ratings[i] = pred;
	}
	var it = VT[0].length;

	// console.log("predictions", pred_ratings);

	//// console.log("loool",pred_ratings[0]);
	//take the position of the top 	N predictions
	var N = 3;
	var val, index;
	var recom_pos = [];
	for (var l = 0; l < N; l++) {
		val = 0;
		for (var i = 0; i < it; i++) {
			//// console.log(pred_ratings[i],">",val);
			if (pred_ratings[i] > val) {
				val = pred_ratings[i];
				index = i;
			}
			//val = pred_ratings[i];
		}
		recom_pos.push(index);
		pred_ratings[index] = 0;
	}
	console.log("positions", recom_pos);
	var recommendations = [];
	for (let i = 0; i < recom_pos.length; i++) {
		recommendations.push(id[recom_pos[i]]);
	}
	return recommendations;
}

async function getVT() {
	const VT = Array.from({ length: factorsNo }, () =>
		Array.from({ length: 5 /*UR.length*/ }, () => 0)
	);

	let docs = await Room.find({}).exec();

	let rooms = {
		count: docs.length,
		rooms: docs.map((doc) => {
			return {
				name: doc.name,
				price: doc.price,
				no_people: doc.no_people,
				elevator: doc.elevator,
				kitchen: doc.kitchen,
				floor: doc.floor,
				room_image: doc.room_image,
				location: doc.location,
				type: doc.type,
				sqmeters: doc.sqmeters,
				heating: doc.heating,
				air_conditioning: doc.air_conditioning,
				wifi: doc.wifi,
				parking: doc.parking,
				television: doc.television,
				no_bathrooms: doc.no_bathrooms,
				no_beds: doc.no_beds,
				no_bedrooms: doc.no_bedrooms,
				living_room: doc.living_room,
				description: doc.description,
				smoking: doc.smoking,
				pets: doc.pets,
				gatherings: doc.gatherings,
				min_no_rentingdays: doc.min_no_rentingdays,
				host_name: doc.host_name,
				host_reviews: doc.host_reviews,
				host_image: doc.host_image,
				_id: doc._id,
			};
		}),
	};

	rooms = rooms.rooms;

	rooms.forEach((room, index) => {
		VT[0][index] = room.no_people
			? room.no_people > 5
				? 1
				: room.no_people / 5 //normalize
			: 0;
		VT[1][index] =
			room.type === "private room"
				? 0
				: room.type === "shared room"
				? 0.5
				: 1;
		VT[2][index] = room.sqmeters
			? room.sqmeters > 500
				? 1
				: room.sqmeters / 500 //normalize
			: 0;
		VT[3][index] = room.floor ? (room.floor > 5 ? 1 : room.floor / 5) : 0; //normalize
		VT[4][index] = room.heating ? 1 : 0;
		VT[5][index] = room.air_conditioning ? 1 : 0;
		VT[6][index] = room.wifi ? 1 : 0;
		VT[7][index] = room.elevator ? 1 : 0;
		VT[8][index] = room.parking ? 1 : 0;
		VT[9][index] = room.kitchen ? 1 : 0;
		VT[10][index] = room.television ? 1 : 0;
		VT[11][index] = room.no_bathrooms
			? room.no_bathrooms > 5
				? 1
				: room.no_bathrooms / 5 //normalize
			: 0;
		VT[12][index] = room.no_beds
			? room.no_beds
				? 1
				: room.no_beds / 5 //normalize
			: 0;
		VT[13][index] = room.no_bedrooms
			? room.no_bedrooms > 5
				? 1
				: room.no_bedrooms / 5 //normalize
			: 0;
		VT[14][index] = room.living_room ? 1 : 0;
		VT[15][index] = room.smoking ? 1 : 0;
		VT[16][index] = room.pets ? 1 : 0;
		VT[17][index] = room.gatherings ? 1 : 0;
		VT[18][index] = room.min_no_rentingdays
			? room.min_no_rentingdays > 5
				? 1
				: room.min_no_rentingdays / 5
			: 0;
		
	});

	return VT;
}

async function getUI(userId) {
	// get rooms
	let docs = await Room.find({}).exec();
	//extract name and id
	let rooms = {
		count: docs.length,
		rooms: docs.map((doc) => {
			return {
				name: doc.name,
				room_id: doc._id,
			};
		}),
	};

	rooms = rooms.rooms;
	const UI = new Array(rooms.length);

	var user = {
		user: userId, //"5f4439498517ab2d8108443d",
	};

	// get user's ratings
	docs = await Rating.find(user);

	const ratings = docs.map((doc) => {
		return {
			room: doc.room,
			rating: doc.rating,
		};
	});
	// TODO: if no ratings get rooms user has viewed
	// for every room
	for (let i = 0; i < rooms.length; i++) {
		// for every rating
		const room = rooms[i];
		UI[i] = null;
		ratings.forEach((rating, index) => {
			// if
			if (rating.room.equals(room.room_id)) {
				UI[i] = rating.rating;
			}
		});
	}

	return [UI, rooms];
}



module.exports = router;
