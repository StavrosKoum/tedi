const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {type: String, required: true},
    surname: {type: String, required: true},
    email: { 
        type: String, 
        required: true, 
        unique: true, 
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    username: { type: String, required: true , unique: true},
    password: { type: String, required: true },
    verify_password: { type: String, required: true },
    phone: { type: String, required: true , unique: true},
    role: { type: String, enum: ["anonymous","tender","host","tenderhost"] , required: true }, //admin already exists in db, we should make dropdown menu with only this 3 choices of roles
    verified: { type: Boolean, default: false }, //only admin is allowed to change this
    city: { type: String, required: false }, //this 3 fields are for anonymous users, to make a quick search
    // rent_days: { type: String, required: false },
    // no_of_tenders: { type: String, required: false }
    rooms_viewed:[ mongoose.Schema.Types.ObjectId],
    //rooms_viewed: { type: [mongoose.Schema.Types.ObjectId], required: false },
    
});

module.exports = mongoose.model('User', userSchema);