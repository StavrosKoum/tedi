const mongoose = require("mongoose");

const roomSchema = mongoose.Schema({
	name: { type: String, required: true },
	_id: mongoose.Schema.Types.ObjectId,
	price: { type: Number, required: true },
	room_image: { type: String, required: false },
	no_people: { type: Number, reqquired: true },
	location: { type: String, required: true },
	type: {
		type: String,
		enum: ["private room", "shared room", "house"],
		required: true,
	},
	sqmeters: { type: Number, required: true },
	floor: { type: Number, required: true }, /// the rest of the info I think host should just be able to put them after he has created the room as "adding info" in the frontend
	heating: { type: Boolean, required: false },
	air_conditioning: { type: Boolean, required: false },
	wifi: { type: Boolean, required: false },
	elevator: { type: Boolean, required: false },
	parking: { type: Boolean, required: false },
	kitchen: { type: Boolean, required: false },
	television: { type: Boolean, required: false },
	no_bathrooms: { type: Number, required: false },
	no_beds: { type: Number, required: false },
	no_bedrooms: { type: Number, required: false },
	living_room: { type: Boolean, required: false },
	description: { type: String, required: false },
	//renting rules:
	smoking: { type: Boolean, required: false },
	pets: { type: Boolean, required: false },
	gatherings: { type: Boolean, required: false },
	min_no_rentingdays: { type: Number, required: false },
	//openstreetmap, buses, neighborhood
	host_name: { type: String, required: true },
	host_reviews: { type: String, required: false },
	host_image: { type: String, required: false },
	image: { type: String, required: false },
});

module.exports = mongoose.model("Room", roomSchema);
