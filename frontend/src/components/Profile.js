import "../App.css";
import Cookie from "js-cookie";
import React, { Component } from "react";
import { Input, Button } from "@material-ui/core";
import axios from "axios";
import { BrowserRouter as Router, Link } from "react-router-dom";
import jwt_decode from 'jwt-decode';


var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}

export default class Profile extends Component {
    constructor(props) {
		super(props);
		this.state = { 

			value: "" ,
			name: decoded.name ,
			surname: "" ,
			username: "" ,
			email: "" ,
			password: "" ,
			verify_Password: "" ,
			phone: "" ,
			city: "" ,
			role: "" ,

		};
        
    }
    
    
render() {
	

	return (
	<div>
		{!Cookie.get("token")?
		(<><h2 > You are not authorized to view this page</h2> </>):
		(<>
		<h1 style={{ fontSize: "3rem" }}>
			<p className="shadow-box-example hoverable">Profile Settings</p>
		</h1>
		
		<div>
			{ Cookie.get("token")
			? (<>
				{ (decoded.role==="admin") ?
					(<>
					<Button>
						<Link to="/Verify" style={{ textDecoration: 'none',textTransform: 'capitalize' }}>Verify Hosts</Link>
					</Button> 
					<Button >
					<Link to="/verifytendershosts" style={{ textDecoration: 'none' ,textTransform: 'capitalize'}}>Verify Tenders/Hosts</Link>
					</Button> 
				
					</>) 
				: 
				(<>
					{(decoded.role==="host" )?(<>
					<Button >
						<Link to="/Add_room" style={{ textDecoration: 'none', textTransform: 'capitalize' }}>Add a room</Link>
					</Button>
					<Button >
					<Link to={{
							pathname: "/Host_rooms",
							searchProp: this.state.name,
						}}
						style={{ textDecoration: 'none',textTransform: 'capitalize' }}
					>
						See your rooms
					</Link>
					</Button> 
					</>):(<>{(decoded.role==="tender" )?(<>
					<Button >
						<Link to="/view_bookings"style={{ textDecoration: 'none',textTransform: 'capitalize' }} >See your bookings </Link>
					</Button>		
					</>):(<>
					{(decoded.role === "tenderhost")?(<>
						<Button >
						<Link to="/Add_room" style={{ textDecoration: 'none', textTransform: 'capitalize' }}>Add a room</Link>
						</Button>
					<Button >
					<Link to={{
							pathname: "/Host_rooms",
							searchProp: this.state.name,
						}}
						style={{ textDecoration: 'none',textTransform: 'capitalize' }}
					>
						See your rooms
					</Link>
					</Button>
					<Button >
						<Link to="/view_bookings"style={{ textDecoration: 'none',textTransform: 'capitalize' }} >See your bookings </Link>
					</Button>
					</>):(<></>)}
					</>)}</>)}
				</>
				)}	 
			</>	
			):(<></>) 
	}	
		</div>
		<Input
		
			style={{
				
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
			label="Name"
			placeholder={decoded.name}
			onChange={(event) => {
				this.setState({
					name: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var user = [
					{
						"propName" :"name" , "value" : this.state.name,
					}];
				
				const userId = decoded.userId;
				console.log(JSON.stringify(user));
				axios
					.patch(
						"https://localhost:3000/user/"+ userId,
						JSON.parse(
							JSON.stringify(user)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>

		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
			label="Surname"
			placeholder={decoded.surname}
			onChange={(event) => {
				this.setState({
					surname: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var user = [
					{
						"propName" :"surname" , "value" : this.state.surname,
					}];
				
				const userId = decoded.userId;
				console.log(JSON.stringify(user));
				axios
					.patch(
						"https://localhost:3000/user/"+ userId,
						JSON.parse(
							JSON.stringify(user)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
			label="Username"
			placeholder={decoded.username}
			onChange={(event) => {
				this.setState({
					username: event.target.value,
				});
			}}
			
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var user = [
					{
						"propName" :"username" , "value" : this.state.username,
					}];
				
				const userId = decoded.userId;
				console.log(JSON.stringify(user));
				axios
					.patch(
						"https://localhost:3000/user/"+ userId,
						JSON.parse(
							JSON.stringify(user)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
			label="email"
			placeholder={decoded.email}
			onChange={(event) => {
				this.setState({
					email: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var user = [
					{
						"propName" :"email" , "value" : this.state.email,
					}];
				
				const userId = decoded.userId;
				console.log(JSON.stringify(user));
				axios
					.patch(
						"https://localhost:3000/user/"+ userId,
						JSON.parse(
							JSON.stringify(user)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
	
		

		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
			label="Phone"
			placeholder={decoded.phone}
			onChange={(event) => {
				this.setState({
					phone: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var user = [
					{
						"propName" :"phone" , "value" : this.state.phone,
					}];
				
				const userId = decoded.userId;
				console.log(JSON.stringify(user));
				axios
					.patch(
						"https://localhost:3000/user/"+ userId,
						JSON.parse(
							JSON.stringify(user)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		<Input
			style={{
				backgroundColor: "white",
				borderRadius: 5
			}}
			className="searchbar"
			type="text"
			label="City"
			placeholder={decoded.city}
			onChange={(event) => {
				this.setState({
					city: event.target.value,
				});
			}}
		/>
		<Button 
		variant="contained" color="secondary"
		onClick={() => {
				var user = [
					{
						"propName" :"city" , "value" : this.state.city,
					}];
				
				const userId = decoded.userId;
				console.log(JSON.stringify(user));
				axios
					.patch(
						"https://localhost:3000/user/"+ userId,
						JSON.parse(
							JSON.stringify(user)
						)
					)
					.then((response) => {
						console.log(response);
					})
					.catch((error) => {
						console.log(error.response);
					});
			}
		}
		>
			Update 
		</Button>
		<br />
		<br />
		</>)}
	</div>
	
	
	);
	
}

}
