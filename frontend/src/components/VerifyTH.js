import "../App.css";
import Cookie from "js-cookie";
import React, { Component } from "react";
import axios from "axios";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import jwt_decode from 'jwt-decode';

var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}

export default class VerifyTH extends Component {

    constructor(props) {
		super(props);

		this.state = {
		    user: [],
		};
    }
    
	componentDidMount = () => {
		axios.get(`https://localhost:3000/user/approvedth`)
        .then((response) => {
			const listings = response.data.user;
			console.log(listings);
			
			this.setState({ user: listings });
			
		});
	};

	render() {
		
		let markersListings = this.state.user.map((data, index) => (
			<div
				style={{ width: "20%", margin: "auto", marginBottom: "1rem" }}
				key={index}
			>
				{!Cookie.get("token") ?(<>
			<h2 >
				You are not authorized to view this page
			</h2> </>):(<>
				{ !(decoded.role==="admin") ?
					(
					<h2 >
						You are not authorized to view this page
					</h2>
					) 
				: 
				(
				<>
				<Card style={{ maxWidth: "145" }}>
					<CardActionArea>
						<CardContent>
							<Typography
								gutterBottom
								variant="h10"
								component="h2"
							>
								{"Name: "}
								{data.name} {data.surname}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"Username: "}
								{data.username}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"email: "}
								{data.email}
							</Typography> 
							 <Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"Phone: "}
								{data.phone}
							</Typography> 
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"City: "}
								{data.city}
							</Typography> 
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"Role: Tender/Host"}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{"Not verified "}
								
							</Typography>
						</CardContent>
					</CardActionArea>
					<CardActions>
						<Button size="small" variant="outlined" color="secondary"
							onClick={() => {
								
								 var newuser = [
								 	{
								 		"propName" :"verified" , "value" : true,
								 	}];
									 const userId=data._id;
								
								axios
									.patch(
										"https://localhost:3000/user/"+ userId,
										JSON.parse(
											JSON.stringify(newuser)
										)
									)
									.then((response) => {
										console.log(response);
										window.location.reload();
									})
									.catch((error) => {
										console.log(error.response);
									});
								
							}}
						>
							Verify
							
						</Button> 
					</CardActions>
				</Card>
				</>)}
				</>)}
			</div>
		));

		return <div className="row">{markersListings}</div>;
	}
    
}