import "../App.css";
import Cookie from "js-cookie";
import React, { Component } from "react";
import { Input, Button } from "@material-ui/core";
import Radio from "@material-ui/core/Radio";
import { BrowserRouter as Link } from "react-router-dom";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import jwt_decode from 'jwt-decode';
import TextField from "@material-ui/core/TextField";

var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}

export default class Add_room extends Component {

  constructor(props) {
		super(props);
		this.state = {
      price: "",
      living_room: "",
      smoking: "",
      pets: "",
      gatherings: "",
      min_no_rentingdays: "",
      host_name: "",
      kitchen: "",
      no_people: "",
      no_bathrooms: "",
      no_beds: "",
      no_bedrooms: "",
			location: "",
			type: "",
			sqmeters: "",
			heating: "",
			floor: "",
			wifi: "",
			television: "",
			elevator: "",
			air_conditioning: "",
      parking: "",
      name: "",
		};

		
	}


  render() 
  {
        return(
        <div>

				<h1 style={{ fontSize: "3rem" }}>
					<p className="shadow-box-example hoverable">Add more filters to your search </p>
				</h1>

              <form noValidate>
						<TextField
							style={{
                backgroundColor: "transparent",
                borderRadius: 5
							}}
							id="date"
						  label="From"
							type="date"
							defaultValue={this.state.start_date}
							InputLabelProps={{
								shrink: true,
							}}
							onChange={(event) => {
								this.setState({
									start_date: event.target.value,
								});
							}}
						/>
					</form>
          <br></br>
          <br></br>
					<form noValidate>
						<TextField
							style={{
                backgroundColor: "transparent",
                borderRadius: 5
							}}
							id="date"
							label="To"
							type="date"
							defaultValue={this.state.end_date}
							InputLabelProps={{
								shrink: true,
							}}
							onChange={(event) => {
								this.setState({
									end_date: event.target.value,
								});
							}}
						/>
					</form> 
           <br></br>
          <br></br> 
				<Input
					style={{
            backgroundColor: "white",
            borderRadius: 5
					}}
					className="searchbar"
					type="text"
					label="Name"
					placeholder="Location"
					onChange={(event) => {
						this.setState({
							location: event.target.value,
						});
					}}
				/> 

				<br />
				<br /> 
		
        <Input
						style={{
							backgroundColor: "white",
              borderRadius: 5
						}}
						className="searchbar"
						type="number"
						label="sqmeters"
						placeholder="Square Meters"
						onChange={(event) => {
							this.setState({
								sqmeters: event.target.value,
							});
						}}
					/>
          <br />
				<br />
        <Input
						style={{
							backgroundColor: "white",
              borderRadius: 5
						}}
						className="searchbar"
						type="number"
						label="floor"
						placeholder="Floor"
						onChange={(event) => {
							this.setState({
								floor: event.target.value,
							});
						}}
					/>

         <br />
				<br />
        <Input
						style={{
							backgroundColor: "white",
              borderRadius: 5
						}}
						className="searchbar"
						type="number"
						
						placeholder="Occupants"
						onChange={(event) => {
							this.setState({
								no_people: event.target.value,
							});
						}}
					/> 
          <br />
				<br />
        <Input
						style={{
							backgroundColor: "white",
              borderRadius: 5
						}}
						className="searchbar"
						type="number"
						
						placeholder="Price per day"
						onChange={(event) => {
							this.setState({
								price: event.target.value,
							});
						}}
					/>
        <br />
				<br />
        <Input
						style={{
							backgroundColor: "white",
              borderRadius: 5
						}}
						className="searchbar"
						type="number"
						
						placeholder="Number of bathrooms"
						onChange={(event) => {
							this.setState({
								no_bathrooms: event.target.value,
							});
						}}
					/>

        <br />
				<br />
        <Input
						style={{
							backgroundColor: "white",
              borderRadius: 5
						}}
						className="searchbar"
						type="number"
						
						placeholder="Number of Beds"
						onChange={(event) => {
							this.setState({
								no_beds: event.target.value,
							});
						}}
					/>

        <br />
				<br />
        <Input
						style={{
							backgroundColor: "white",
              borderRadius: 5
						}}
						className="searchbar"
						type="number"
						
						placeholder="Number of Bedrooms"
						onChange={(event) => {
							this.setState({
								no_bedrooms: event.target.value,
							});
						}}
					/>

          <br />
          <br />
          <Input
              style={{
                backgroundColor: "white",
                borderRadius: 5
              }}
              className="searchbar"
              type="number"
              
              placeholder="Minimum Rent Days"
              onChange={(event) => {
                this.setState({
                  min_no_rentingdays: event.target.value,
                });
              }}
            />

          <br />
          <br />
    
				<br />
				<br />
				<div>
					<form >
						<FormControl
							component="fieldset"
							
							style={{
                backgroundColor: "white",
                borderRadius: 5
							}}
						>
							<FormLabel
								component="legend"
								style={{
                  backgroundColor: "white",
                  borderRadius: 5
								}}
							>
								Choose room type
              
							</FormLabel>
							
							<RadioGroup
								
								onChange={(event) => {
									this.setState({
										type: event.target.value,
									});
								}}
							>
								<FormControlLabel
									value="private room"
									variant="contained"
									control={<Radio />}
									label="private room"
								/>
								<FormControlLabel
									value="shared room"
									control={<Radio />}
									label="shared room"
								/>
								<FormControlLabel
									value="house"
									control={<Radio />}
									label="house"
								/>
							</RadioGroup>
						</FormControl>
					</form>
				</div>
				<br />
				<br />
        <div>
					<form >
						<FormControl
							component="fieldset"
							
							style={{
                backgroundColor: "white",
                borderRadius: 5
							}}
						>
							<FormLabel
								component="legend"
								style={{
                  backgroundColor: "white",
                  borderRadius: 5
								}}
							>
								Heating
              
							</FormLabel>
							
							<RadioGroup
								
								onChange={(event) => {
									this.setState({
										heating: event.target.value,
									});
								}}
							>
								<FormControlLabel
									value="true"
									variant="contained"
									control={<Radio />}
									label="yes "
								/>
								<FormControlLabel
									value="false"
									control={<Radio />}
									label="no"
								/>
							</RadioGroup>
						</FormControl>
					</form>
				</div>
        <br />
				<br />
        <form >
        <FormControl
          component="fieldset"
        
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Wifi
          
          </FormLabel>
          
          <RadioGroup
            
            onChange={(event) => {
              this.setState({
                wifi: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>
				

      <br />
			<br />

      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Television
          
          </FormLabel>
          
          <RadioGroup
            
            onChange={(event) => {
              this.setState({
                television: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>


      <br />
			<br />

      <form >
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Elevator
          
          </FormLabel>
          
          <RadioGroup
            
            onChange={(event) => {
              this.setState({
                elevator: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>

      <br />
			<br />
      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Air Conditioning
          
          </FormLabel>
          
          <RadioGroup
            onChange={(event) => {
              this.setState({
                air_conditioning: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>


      <br />
			<br />
      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Parking
          
          </FormLabel>
          
          <RadioGroup
            onChange={(event) => {
              this.setState({
                parking: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>


      <br />
			<br />
      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Kitchen
          
          </FormLabel>
          
          <RadioGroup
            onChange={(event) => {
              this.setState({
                kitchen: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>



      <br />
			<br />
      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Living_room
          
          </FormLabel>
          
          <RadioGroup
            onChange={(event) => {
              this.setState({
                living_room: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>
      <br />
			<br />
      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Smoking
          
          </FormLabel>
          
          <RadioGroup
            onChange={(event) => {
              this.setState({
                smoking: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>


      <br />
			<br />
      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Pets
          
          </FormLabel>
          
          <RadioGroup
            onChange={(event) => {
              this.setState({
                pets: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>

      <br />
			<br />
      <form>
        <FormControl
          component="fieldset"
          
          style={{
            backgroundColor: "white",
            borderRadius: 5
          }}
        >
          <FormLabel
            component="legend"
            style={{
              backgroundColor: "white",
              borderRadius: 5
            }}
          >
            Gatherings allowed
          
          </FormLabel>
          
          <RadioGroup
            onChange={(event) => {
              this.setState({
                gatherings: event.target.value,
              });
            }}
          >
            <FormControlLabel
              value="true"
              variant="contained"
              control={<Radio />}
              label="yes "
            />
            <FormControlLabel
              value="false"
              control={<Radio />}
              label="no"
            />
          </RadioGroup>
        </FormControl>
      </form>
      <br></br>
      <br></br>
      <Button variant="contained" color="secondary">
							<Link
								to={{
									pathname: "/more_filters",
									searchProp: this.state,
                }}
                style={{ textDecoration: 'none',textTransform: 'capitalize' }}
							>
								Search
							</Link>
						</Button>
      
      <br></br>
      <br></br>
        

			</div>
        );
    }
}