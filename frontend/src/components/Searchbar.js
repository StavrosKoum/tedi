import React, { Component } from "react";
import { Input, Button } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import Cookie from "js-cookie";
import jwt_decode from 'jwt-decode';
import "../App.css";


var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}


export default class Searchbar extends Component {
	constructor(props) {
		super(props);
		var today = new Date(),
		date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	
		this.state = {
			location: "",
			no_people: "",
			start_date: date,
			end_date: date, 
		};
	}

	render() {
		return (
			<div>
				<h1 style={{ fontSize: "5rem" }}>
					<p className="dropShadow">Welcome!</p>
				</h1>
			<div>
			<Input
				style={{
					backgroundColor: "white",
					borderRadius: 5,
					
				}}
				className="searchbar"
				type="text"
				label="Location"
				placeholder="Location"
				onChange={(event) => {
					this.setState({
						location: event.target.value,
					});
				}}
			/>
			<br />
			<br />
			<Input
				style={{
					backgroundColor: "white",
					borderRadius: 5
				}}
				className="searchbar"
				type="number"
				label="Occupants"
				placeholder="Occupants"
				onChange={(event) => {
					this.setState({
						no_people: event.target.value,
					});
				}}
			/>
			<br />
			<br />
			<form noValidate>
				<TextField
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					id="date"
					label="From"
					type="date"
					InputLabelProps={{
						shrink: true,
					}}
					onChange={(event) => {
						this.setState({
							start_date: event.target.value,
						});
					}}
				/>
			</form>
			<br />
			<form noValidate>
				<TextField
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					id="date"
					label="To"
					type="date"
					defaultValue={this.state.end_date}
					InputLabelProps={{
						shrink: true,
					}}
					onChange={(event) => {
						this.setState({
							end_date: event.target.value,
						});
					}}
				/>
			</form>
			<br />
			<br />
			<div>
				<Button variant="contained" color="secondary">
					<Link
						to={{
							pathname: "/result",
							searchProp: this.state,
						}}
						style={{ textDecoration: 'none',textTransform: 'capitalize' }}
					>
						Search
					</Link>
				</Button>
				<br />
				<br />
				<Button variant="contained" color="secondary">
					<Link
						to={{
							pathname: "/filters" ,
						}}
						style={{ textDecoration: 'none',textTransform: 'capitalize' }}
					>
						Search with more filters
					</Link>
				</Button>
				<br />
				<br />
				<Button variant="contained" color="secondary">
					<Link to="/rooms"style={{ textDecoration: 'none',textTransform: 'capitalize' }}>Browse</Link>
				</Button>
				<br />
				<br />
				<div>
				{Cookie.get("token")?(<>
				<Button variant="contained" color="secondary">
					<Link to="/recommendations"style={{ textDecoration: 'none',textTransform: 'capitalize' }}>Show recommendations</Link>
				</Button></>)
				:<>("")</>}
				</div>
				</div>
			</div>
		</div>
		);
	}
}
