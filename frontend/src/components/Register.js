import React, { Component } from "react";
import { Input, Button } from "@material-ui/core";
import { BrowserRouter as Link } from "react-router-dom";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import axios from "axios";
import Cookie from "js-cookie";
import jwt_decode from 'jwt-decode';

var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
	console.log(decoded);}
	
function check() {
	var a = document.getElementById("passwords").value;
	var b = document.getElementById("confpasswords").value;
	if (a !== b) {
		document.getElementById("messages").innerHTML =
			"passwords are not the same";
	} else {
		document.getElementById("messages").innerHTML = "passwords are valid";
	}
}

export default class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: "",
			name: "",
			surname: "",
			username: "",
			email: "",
			password: "",
			verify_Password: "",
			phone: "",
			city: "",
			role: "",
		};

	}

	render() {
		return (
			<div>
				<h1 style={{ fontSize: "3rem" }}>
					<p className="shadow-box-example hoverable">Register</p>
				</h1>

				<Input
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					className="searchbar"
					type="text"
					label="Name"
					placeholder="Name"
					onChange={(event) => {
						this.setState({
							name: event.target.value,
						});
					}}
				/>

				<br />
				<br />
				<Input
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					className="searchbar"
					type="text"
					label="Surname"
					placeholder="Surname"
					onChange={(event) => {
						this.setState({
							surname: event.target.value,
						});
					}}
				/>
				<br />
				<br />
				<Input
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					className="searchbar"
					type="text"
					label="Username"
					placeholder="Username"
					onChange={(event) => {
						this.setState({
							username: event.target.value,
						});
					}}
				/>
				<br />
				<br />
				<Input
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					className="searchbar"
					type="text"
					label="email"
					placeholder="email"
					onChange={(event) => {
						this.setState({
							email: event.target.value,
						});
					}}
				/>
				<br />
				<br />
				<form>
					<Input
						id="passwords"
						style={{
							backgroundColor: "white",
							borderRadius: 5
						}}
						type="password"
						label="Password"
						placeholder="Password"
						onChange={(event) => {
							this.setState({
								password: event.target.value,
							});
						}}
					/>
					<br />
					<br />

					<Input
						id="confpasswords"
						style={{
							backgroundColor: "white",
							borderRadius: 5
						}}
						type="password"
						label="Password"
						placeholder="Verify Password"
						onChange={(event) => {
							this.setState({
								password: event.target.value,
							});
							check();
						}}
					/>

					<span id="messages" style={{ background: "white" }}></span>
				</form>
				<br />

				<Input
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					className="searchbar"
					type="text"
					label="Phone"
					placeholder="Phone"
					onChange={(event) => {
						this.setState({
							phone: event.target.value,
						});
					}}
				/>
				<br />
				<br />
				<Input
					style={{
						backgroundColor: "white",
						borderRadius: 5
					}}
					className="searchbar"
					type="text"
					label="City"
					placeholder="City"
					onChange={(event) => {
						this.setState({
							city: event.target.value,
						});
					}}
				/>
				<br />
				<br />
				<div>
					<form >
						<FormControl
							component="fieldset"
							style={{
								backgroundColor: "white",
								borderRadius: 5
							}}
						>
							<FormLabel
								component="legend"
								style={{
									backgroundColor: "white",
									borderRadius: 5
								}}
							>
								Role
							</FormLabel>
							
							<RadioGroup
								aria-label="role"
								name="role" 
								onChange={(event) => {
									this.setState({
										role: event.target.value,
									});
								}}
							>
								<FormControlLabel
									value="tender"
									variant="contained"
									control={<Radio />}
									label="Tender"
								/>
								<FormControlLabel
									value="host"
									control={<Radio />}
									label="Host"
								/>
								<FormControlLabel
									value="tenderhost"
									control={<Radio />}
									label="Both"
								/>
							</RadioGroup>
						</FormControl>
					</form>
				</div>
				<br />
				<br />
				
				<Button
					variant="contained"
					color="secondary"
					onClick={() => {
						if (this.state.email && this.state.password) {
							const user = {
								name: this.state.name,
								surname: this.state.surname,
								email: this.state.email,
								username: this.state.username,
								password: this.state.password,
								verify_password: this.state.verify_password,
								phone: this.state.phone,
								city: this.state.city,
								role: this.state.role,
							};
							const user2 = {
								email: this.state
									.email,
								password: this.state
									.password,
							};
							axios
								.post(
									"https://localhost:3000/user/signup",
									user
								)
								.then((response) => {
									alert(response.data.message);
									axios
									.post(
										"https://localhost:3000/user/login",
										user2
									)
									.then(
										(response) => {
											if (response.data.message === "Auth successful"	) 
											{
												Cookie.set(
													"token",
													response.data.token
												);
												window.location.reload();
											}
											else{
												alert(response.data.message);
											}
										}
									)
									.catch((error) => {
										console.log(
											error.response
										);
									});
								})
								.catch((error) => {
									alert(error.response.data.message);
								});

								
						}
					}}
				>
					<Link to="/"style={{ textDecoration: 'none',textTransform: 'capitalize' }}>Register</Link>
					
				</Button>
				<br />
			</div>
		);
	}
}
