import "../App.css";
import Cookie from "js-cookie";
import React, { Component } from "react";
import { Button } from "@material-ui/core";
import axios from "axios";
import { BrowserRouter as Link } from "react-router-dom";
import jwt_decode from 'jwt-decode';
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import Typography from "@material-ui/core/Typography";


var token =Cookie.get("token");
if (token) {
var decoded = jwt_decode(token);
    console.log(decoded);}


export default class Recommendations extends Component {
    constructor(props) {
		super(props);
		this.state = { 
            recomm: null,
            
        };
        
    
        
    }
    componentDidMount = () => {

        axios.get(`https://localhost:3000/ratings/`+decoded.userId)
        .then((response) => {
            console.log("in get ratings",response);
            
            if(response.data.count){    //if ratings exist
                axios.get(`https://localhost:3000/matrixfact/`+decoded.userId) //print the recommended rooms
                .then((response) => {
                    console.log(response);
                    const listings = response.data.recommendations;
			        this.setState({ recomm: listings });
                })
                .catch((error) => {
                    console.log(error.response);
                });
            }
            else //if user has not ratings 
            {      
                axios.get(`https://localhost:3000/noRating/`+decoded.userId) //print the recommended rooms
                .then((rooms) => {
                    if(rooms){
                    const listings = rooms.data.recommendations;
                    this.setState({ recomm: listings });}
                    else{}
                })
                .catch((error) => {
                    console.log(error.response);
                });         
            }
        })
        .catch((error) => {
            console.log(error.response);
        });

		}
	
    
    render() {         
            
        if (this.state.recomm){	
            
        let markersListings = this.state.recomm.map((data, index) => (
            
			<div
				style={{ width: "80%", margin: "auto", marginBottom: "2rem" }}
				key={index}
			>
                
               
				<Card style={{ maxWidth: "345" }}>
					<CardActionArea>
						<CardMedia
							component="img"
                            alt="Apartment image"
                            height="345"
                            image={data.image ? data.image : "logo512.png"}
                            title="Apartment image"
                            onClick={() => {
                                window.location = `/rooms/${data._id}`;
                            }}
						/>
						<CardContent>
							<Typography
								gutterBottom
								variant="h5"
								component="h2"
							>
								{data.name}
							</Typography>
							<Typography
								variant="body2"
								color="textSecondary"
								component="p"
							>
								{data.description}
							</Typography>
						</CardContent>
					</CardActionArea>
					<CardActions>
						<Button size="small" color="primary"
				
					   
				>
						<Link to={{pathname:`/rooms/${data.room_id}`, idprop:data._id}}
                        style={{
                            textDecoration: "none",
                            textTransform: "capitalize",
                        }}>View
						</Link>
						</Button>
					</CardActions>
				</Card>
			</div>
		));

        return <div className="row">{markersListings}</div>;
        }
        else 
        {  
           return<div
            style={{ width: "80%", margin: "auto", marginBottom: "2rem" }}
            >
            <Card style={{ maxWidth: "345" }}>
                 <CardActionArea>
                     <CardContent>
                         <Typography
                             gutterBottom
                             variant="h5"
                             component="h2"
                         >
                             Sorry, there are no recommendations
                     </Typography>
                        
                     </CardContent>
                 </CardActionArea>
             </Card>
         </div>
        }
     }


}

